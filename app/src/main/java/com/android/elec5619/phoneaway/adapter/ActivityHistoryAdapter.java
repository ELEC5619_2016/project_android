package com.android.elec5619.phoneaway.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.elec5619.phoneaway.R;
import com.android.elec5619.phoneaway.domain.History;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ASUS on 2016/10/16.
 */

public class ActivityHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<String> listTag = null;
    private LayoutInflater layoutInflater;
    private ArrayList<History> histories ;

    public ActivityHistoryAdapter(Context context, ArrayList<History> histories) {

        this.context = context;
        this.histories = histories;
        this.layoutInflater = LayoutInflater.from(context);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return histories == null ? 0 : histories.size();
    }

    private int type_normal = 0;
    private int type_split = 1;


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == type_normal) {
            return new ViewHolder(layoutInflater.inflate(R.layout.item_history, parent, false));
        } else {
            return new ViewHolderSplit(layoutInflater.inflate(R.layout.item_split_history, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        History history = histories.get(position);
        if (holder instanceof ViewHolder){
            ((ViewHolder) holder).textViewTop.setText(history.getTop());
            ((ViewHolder) holder).textViewBottom.setText(history.getBottom()+" s");
        } else if (holder instanceof ViewHolderSplit) {
            ((ViewHolderSplit) holder).textViewDate.setText("" + history.getTimeSp());
        }
    }

    @Override
    public int getItemViewType(int position) {

        History history = histories.get(position);

        if (history.isSplite()) {
            return type_split;
        } else {
            return type_normal;
        }

    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.textViewContentTop)
        TextView textViewTop;

        @Bind(R.id.textViewContentBottom)
        TextView textViewBottom;

        @Bind(R.id.imageViewRightGo)
        ImageView imageViewRightGo;

        @Bind(R.id.viewRoot)
        View viewRoot;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

    }


    static class ViewHolderSplit extends RecyclerView.ViewHolder{

        @Bind(R.id.textViewDate)
        TextView textViewDate;

        public ViewHolderSplit(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }



}

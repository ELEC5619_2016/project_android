package com.android.elec5619.phoneaway.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2016/10/24.
 */

public class Step implements Serializable{
    private String uid;
    private String date;
    private String steps;

    private String sdate;
    private String edate;

    private List<OffTime> list;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public String getSdate() {
        return sdate;
    }

    public void setSdate(String sdate) {
        this.sdate = sdate;
    }

    public String getEdate() {
        return edate;
    }

    public void setEdate(String edate) {
        this.edate = edate;
    }

    public List<OffTime> getList() {
        return list;
    }

    public void setList(List<OffTime> list) {
        this.list = list;
    }
}

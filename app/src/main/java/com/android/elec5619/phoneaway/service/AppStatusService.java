package com.android.elec5619.phoneaway.service;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Icon;
import android.net.ConnectivityManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.elec5619.phoneaway.R;
import com.android.elec5619.phoneaway.activity.HistoryActivity;
import com.android.elec5619.phoneaway.activity.MainActivity;
import com.android.elec5619.phoneaway.util.BackgroundUtil;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class AppStatusService extends Service {
    private Context mContext;
    private Notification notification;
    private AlarmManager manager;
    private PendingIntent pendingIntent;
    private NotificationCompat.Builder mBuilder;
    private Intent mIntent;
    private NotificationManager mNotificationManager;
    private static final int NOTICATION_ID = 0x1;

    public interface GetForegroundApp {
        void GetPackageName(String packagename);
    }

    private GetForegroundApp onGetForegroundApp;

    public void setOnGetForegroundApp(GetForegroundApp onGetForegroundApp) {
        this.onGetForegroundApp = onGetForegroundApp;
    }

    private Binder binder = new MyBinder();
    private Timer timer;
    public class MyBinder extends Binder {
        public AppStatusService getService() {
            return AppStatusService.this;
        }
    }

    class MyTimerTask extends TimerTask {
        private Context context;

        public MyTimerTask(Context context) {
            this.context = context;
        }

        @Override
        public void run() {
            String packagename = BackgroundUtil.getForegroundApp(context);

            if (onGetForegroundApp != null) {
                onGetForegroundApp.GetPackageName(BackgroundUtil.getForegroundApp(context));
                Log.e("Notify App Pkgname:", ""+BackgroundUtil.getForegroundApp(context));
                if(packagename != null){
                    if(!packagename.equals("com.android.elec5619.stayfocus")){
                        startNotification(packagename);
                    }
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void startNotification(String packagename) {

        mIntent = new Intent(mContext, HistoryActivity.class);
        pendingIntent = PendingIntent.getActivity(mContext, 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentText("Because of " + packagename)
                .setContentTitle("Your Goal Failed")
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);
        notification = mBuilder.build();
        startForeground(NOTICATION_ID, notification);
    }

    public AppStatusService() {
    }

    @Override
    public IBinder onBind(Intent intent) {

        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Log.i("AppStatusService", "create()");
        timer = new Timer();
        timer.schedule(new MyTimerTask(getApplicationContext()), new Date(), 2000);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("AppStatusService", "destroy()");
        if(timer != null ){
            timer.cancel();
            timer = null;
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("AppStatusService", "start()");
        return super.onStartCommand(intent, flags, startId);
    }
}

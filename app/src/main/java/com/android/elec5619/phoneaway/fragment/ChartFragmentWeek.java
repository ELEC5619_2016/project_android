package com.android.elec5619.phoneaway.fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.elec5619.phoneaway.App;
import com.android.elec5619.phoneaway.R;
import com.android.elec5619.phoneaway.activity.BaseActivity;
import com.android.elec5619.phoneaway.activity.LoginActivity;
import com.android.elec5619.phoneaway.activity.MainActivity;
import com.android.elec5619.phoneaway.domain.OffTime;
import com.android.elec5619.phoneaway.domain.User;
import com.android.elec5619.phoneaway.net.HttpHelper;
import com.android.elec5619.phoneaway.util.TimeUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lecho.lib.hellocharts.listener.ColumnChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.ColumnChartView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChartFragmentWeek extends Fragment {
    private static final String ARG_PARAM1 = "Chart_Page";

    private static int TYPE = 0;
    private List<OffTime> offTimes = new ArrayList<>();
    private List<AxisValue> mAxisXValues = new ArrayList<AxisValue>();

    public ChartFragmentWeek() {
        // Required empty public constructor
    }

    public static ChartFragmentWeek newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, page);
        TYPE = page;
        Log.i("ChartPage", ""+page);
        ChartFragmentWeek pageFragment = new ChartFragmentWeek();
        pageFragment.setArguments(args);
        return pageFragment;
    }


    private static final int DEFAULT_DATA = 0;
    private static final int SUBCOLUMNS_DATA = 1;
    private static final int STACKED_DATA = 2;
    private static final int NEGATIVE_SUBCOLUMNS_DATA = 3;
    private static final int NEGATIVE_STACKED_DATA = 4;

    private ColumnChartView chart;
    private ColumnChartData data;
    private boolean hasAxes = true;
    private boolean hasAxesNames = true;
    private boolean hasLabels = false;
    private boolean hasLabelForSelected = false;
    private int dataType = DEFAULT_DATA;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_chart, container, false);

        chart = (ColumnChartView) rootView.findViewById(R.id.chart);
        chart.setOnValueTouchListener(new ValueTouchListener());

        getWeekData();

        return rootView;
    }

    private void getWeekData(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final OffTime offTime = new OffTime();
        offTime.setUid(App.getApp().getUserId());
        try {
            offTime.setSdate(TimeUtil.getStartTime());
            offTime.setEdate(TimeUtil.getEndTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        HttpHelper.NetService service = retrofit.create(HttpHelper.NetService.class);
        retrofit2.Call<OffTime> call = service.getOffTimeList(offTime);
        call.enqueue(new Callback<OffTime>() {
                         @Override
                         public void onResponse(Call<OffTime> call, Response<OffTime> response) {
                             if(response.code()== 200){
                                 offTimes = response.body().getList();
                                 generateDefaultData();
                                 Log.i("OFFTIME", response.body()+"");
                             }
                         }

                         @Override
                         public void onFailure(Call<OffTime> call, Throwable t) {
                             if (call.isCanceled()) {
                                 Log.d("", "the call is canceled , " + toString());
                             } else {
                                 Log.e("", t.toString());
                             }
                         }
                     });
    }

    private void getAxisXLables(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 0; i < offTimes.size(); i++) {
            mAxisXValues.add(new AxisValue(i).setLabel(sdf.format(new Date(Long.parseLong(offTimes.get(i).getDate())))));
        }
    }

    private void generateDefaultData() {
        int numSubcolumns = 1;
        int numColumns = 7;
        if(offTimes != null){
            numColumns = offTimes.size();
        }

        List<Column> columns = new ArrayList<Column>();
        List<SubcolumnValue> values;
        for (int i = 0; i < numColumns; ++i) {

            values = new ArrayList<SubcolumnValue>();
            for (int j = 0; j < numSubcolumns; ++j) {
                values.add(new SubcolumnValue(Float.valueOf(offTimes.get(i).getLength()), ChartUtils.pickColor()));
            }

            Column column = new Column(values);
            column.setHasLabels(hasLabels);
            column.setHasLabelsOnlyForSelected(hasLabelForSelected);
            columns.add(column);
        }

        data = new ColumnChartData(columns);

        getAxisXLables();
        if (hasAxes) {
            Axis axisX = new Axis();
            axisX.setValues(mAxisXValues);
            Axis axisY = new Axis().setHasLines(true);
            if (hasAxesNames) {
                axisX.setName("");
                axisY.setName("Seconds");
            }
            data.setAxisXBottom(axisX);
            data.setAxisYLeft(axisY);
        } else {
            data.setAxisXBottom(null);
            data.setAxisYLeft(null);
        }

        chart.setColumnChartData(data);

    }

    private class ValueTouchListener implements ColumnChartOnValueSelectListener {

        @Override
        public void onValueSelected(int columnIndex, int subcolumnIndex, SubcolumnValue value) {
            Toast.makeText(getActivity(), "Selected: " + value, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onValueDeselected() {
            // TODO Auto-generated method stub

        }

    }

}


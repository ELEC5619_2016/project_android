package com.android.elec5619.phoneaway.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by ASUS on 2016/10/8.
 */

public class OffTime implements Serializable{

    private String uid;
    private String date;
    private String done;
    private String length;

    private String sdate;
    private String edate;

    private List<OffTime> list;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String odate) {
        this.date = odate;
    }

    public String getDone() {
        return done;
    }

    public void setDone(String done) {
        this.done = done;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getSdate() {
        return sdate;
    }

    public void setSdate(String sdate) {
        this.sdate = sdate;
    }

    public String getEdate() {
        return edate;
    }

    public void setEdate(String edate) {
        this.edate = edate;
    }

    public List<OffTime> getList() {
        return list;
    }

    public void setList(List<OffTime> list) {
        this.list = list;
    }
}

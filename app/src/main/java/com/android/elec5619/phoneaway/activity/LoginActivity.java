package com.android.elec5619.phoneaway.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.elec5619.phoneaway.App;
import com.android.elec5619.phoneaway.R;
import com.android.elec5619.phoneaway.domain.User;
import com.android.elec5619.phoneaway.net.HttpHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends BaseActivity {

    @Bind(R.id.editTextUser)
    EditText editTextUser;

    @Bind(R.id.editTextpwd)
    EditText editTextPwd;

    @Bind(R.id.buttonSignUp)
    Button buttonSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        buttonSignUp.setVisibility(View.GONE);
    }

    @OnClick(R.id.buttonSignIn)
    public void onSignInClick() {
        login();

    }

    @OnClick(R.id.buttonSignUp)
    public void onSignUpClick() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private void login() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        User user = new User();
        user.setUsername(editTextUser.getText().toString());
        user.setPassword(editTextPwd.getText().toString());

        HttpHelper.NetService service = retrofit.create(HttpHelper.NetService.class);
        retrofit2.Call<User> call = service.login(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.i("Login", response.code() + "," +  response.body().getUsername() + response.body().getPassword() + response.body());
                if(response.code() == 200 && response.body().getUsername()!= null){
                    App.getApp().setUserId(response.body().getId());
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("username", response.body().getUsername());
                    startActivity(intent);
                }else{
                    Log.i("Login", "Login Failed");

                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setMessage(getResources().getString(R.string.error_incorrect_password));
                    builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    builder.create().show();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

                if (call.isCanceled()) {
                    Log.d("", "the call is canceled , " + toString());
                } else {
                    Log.e("", t.toString());
                }
            }
        });

    }
}
package com.android.elec5619.phoneaway.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.elec5619.phoneaway.R;
import com.android.elec5619.phoneaway.view.CircleProgressBar;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SettingActivity extends AppCompatActivity {
    @Bind(R.id.textViewGoals)
    TextView textViewGoals;

    @Bind(R.id.seekBarTime)
    SeekBar seekBarTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        init();
    }

    private void init() {
        seekBarTime.setProgress(CircleProgressBar.max / 60);
        textViewGoals.setText( CircleProgressBar.max / 60 +"minutes");
        seekBarTime.setOnSeekBarChangeListener(seekListener);
    }

    private SeekBar.OnSeekBarChangeListener seekListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            Log.i("Seekbar", "onStopTrackingTouch");
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            Log.i("Seekbar", "onStartTrackingTouch");
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress,
                                      boolean fromUser) {
            Log.i("Seekbar", "onProgressChanged");
            textViewGoals.setText( progress +"minutes");
            CircleProgressBar.max = progress*60;

        }
    };
}

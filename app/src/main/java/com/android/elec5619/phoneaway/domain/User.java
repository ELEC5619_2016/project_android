package com.android.elec5619.phoneaway.domain;

import java.io.Serializable;

/**
 * Created by ASUS on 2016/9/13.
 */
public class User implements Serializable{

    private String id;
    private String username;
    private String password;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

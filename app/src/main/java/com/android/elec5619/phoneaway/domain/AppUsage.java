package com.android.elec5619.phoneaway.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2016/10/22.
 */

public class AppUsage implements Serializable{
    private String uid;
    private String adate;
    private String aname;
    private String count;

    private String sdate;
    private String edate;
    private List<AppUsage> list;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getAdate() {
        return adate;
    }

    public void setAdate(String adate) {
        this.adate = adate;
    }

    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    public String getSdate() {
        return sdate;
    }

    public void setSdate(String sdate) {
        this.sdate = sdate;
    }

    public String getEdate() {
        return edate;
    }

    public void setEdate(String edate) {
        this.edate = edate;
    }

    public List<AppUsage> getList() {
        return list;
    }

    public void setList(List<AppUsage> list) {
        this.list = list;
    }
}

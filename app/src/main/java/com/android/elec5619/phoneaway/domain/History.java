package com.android.elec5619.phoneaway.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by ASUS on 2016/10/16.
 */

public class History implements Serializable {
    private String id;
    private String top;
    private String bottom;
    private Date time;
    private String url;

    private boolean isSplite;
    private  String timeSp;

    public String getTop() {
        return top;
    }

    public void setTop(String top) {
        this.top = top;
    }

    public String getBottom() {
        return bottom;
    }

    public void setBottom(String bottom) {
        this.bottom = bottom;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }


    public boolean isSplite() {
        return isSplite;
    }

    public void setSplite(boolean splite) {
        isSplite = splite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTimeSp() {
        return timeSp;
    }

    public void setTimeSp(String timeSp) {
        this.timeSp = timeSp;
    }
}

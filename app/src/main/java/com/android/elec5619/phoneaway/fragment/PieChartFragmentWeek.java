package com.android.elec5619.phoneaway.fragment;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.elec5619.phoneaway.App;
import com.android.elec5619.phoneaway.R;
import com.android.elec5619.phoneaway.activity.BaseActivity;
import com.android.elec5619.phoneaway.domain.AppUsage;
import com.android.elec5619.phoneaway.net.HttpHelper;
import com.android.elec5619.phoneaway.util.TimeUtil;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.listener.PieChartOnValueSelectListener;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.PieChartView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PieChartFragmentWeek extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters
    private PieChartView chart;
    private PieChartData data;

    private boolean hasLabels = true;
    private boolean hasLabelsOutside = false;
    private boolean hasCenterCircle = false;
    private boolean hasCenterText1 = false;
    private boolean hasCenterText2 = false;
    private boolean isExploded = false;
    private boolean hasLabelForSelected = false;

    private List<AppUsage> appUsages = new ArrayList<>();
    private List<AxisValue> mAxisXValues = new ArrayList<AxisValue>();

    public PieChartFragmentWeek() {

    }

    public static PieChartFragmentWeek newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, page);
        PieChartFragmentWeek pageFragment = new PieChartFragmentWeek();
        pageFragment.setArguments(args);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pie_chart, container, false);

        chart = (PieChartView) rootView.findViewById(R.id.pieChart);
        chart.setOnValueTouchListener(new ValueTouchListener());

        getData();
        return rootView;
    }

    private void getData(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final AppUsage appUsage = new AppUsage();
        appUsage.setUid(App.getApp().getUserId());
        appUsage.setAname("");
        try {
            appUsage.setSdate(TimeUtil.getStartTime());
            appUsage.setEdate(TimeUtil.getEndTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        HttpHelper.NetService service = retrofit.create(HttpHelper.NetService.class);
        Call<AppUsage> call = service.getAppUsageList(appUsage);
        call.enqueue(new Callback<AppUsage>() {
            @Override
            public void onResponse(Call<AppUsage> call, Response<AppUsage> response) {
                if(response.code()== 200){
                    appUsages = response.body().getList();
                    generateData();
                    Log.i("AppUsage", response.body()+"");
                }
            }

            @Override
            public void onFailure(Call<AppUsage> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.d("", "the call is canceled , " + toString());
                } else {
                    Log.e("", t.toString());
                }
            }
        });
    }

    private void generateData() {
        int numValues = appUsages.size();

        List<SliceValue> values = new ArrayList<SliceValue>();
        for (int i = 0; i < numValues; ++i) {
            SliceValue sliceValue = new SliceValue(Float.valueOf(appUsages.get(i).getCount()), ChartUtils.pickColor());
            sliceValue.setLabel(appUsages.get(i).getAname());
            values.add(sliceValue);
        }

        data = new PieChartData(values);
        data.setHasLabels(hasLabels);
        data.setHasLabelsOnlyForSelected(hasLabelForSelected);
        data.setHasLabelsOutside(hasLabelsOutside);
        data.setHasCenterCircle(hasCenterCircle);

        if (isExploded) {
            data.setSlicesSpacing(24);
        }

        if (hasCenterText1) {
            data.setCenterText1("Hello!");

            // Get roboto-italic font.
            Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Italic.ttf");
            data.setCenterText1Typeface(tf);

            // Get font size from dimens.xml and convert it to sp(library uses sp values).
//            data.setCenterText1FontSize(ChartUtils.px2sp(getResources().getDisplayMetrics().scaledDensity,
//                    (int) getResources().getDimension(R.dimen.pie_chart_text1_size)));
        }

        if (hasCenterText2) {
            data.setCenterText2("Charts (Roboto Italic)");

            Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Italic.ttf");

            data.setCenterText2Typeface(tf);
//            data.setCenterText2FontSize(ChartUtils.px2sp(getResources().getDisplayMetrics().scaledDensity,
//                    (int) getResources().getDimension(R.dimen.pie_chart_text2_size)));
        }

        chart.setPieChartData(data);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private class ValueTouchListener implements PieChartOnValueSelectListener {

        @Override
        public void onValueSelected(int arcIndex, SliceValue value) {
            Toast.makeText(getActivity(), "Selected: " + value, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onValueDeselected() {
            // TODO Auto-generated method stub

        }

    }

}

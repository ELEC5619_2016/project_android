package com.android.elec5619.phoneaway.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telecom.Call;
import android.util.Log;
import android.widget.TextView;

import com.android.elec5619.phoneaway.App;
import com.android.elec5619.phoneaway.R;
import com.android.elec5619.phoneaway.adapter.ActivityHistoryAdapter;
import com.android.elec5619.phoneaway.domain.AppUsage;
import com.android.elec5619.phoneaway.domain.History;
import com.android.elec5619.phoneaway.domain.OffTime;
import com.android.elec5619.phoneaway.net.HttpHelper;

import java.util.ArrayList;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Callback;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HistoryActivity extends BaseActivity {
    @Bind(R.id.recyclerViewHistory)
    RecyclerView recyclerViewHistoy;

    @Bind(R.id.textViewOfftimeLenth)
    TextView  textViewOffTime;

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ArrayList<History> histories = new ArrayList<>();

    private App app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);
        app = (App) getApplication();
        init();
        if(App.getApp().getAppUsage()!= null){
            uploadApp();
        }
        uploadTime();
    }
    private void init(){
        recyclerViewHistoy.setHasFixedSize(true);
        textViewOffTime.setText(App.getApp().getLength());

        mLayoutManager = new LinearLayoutManager(this);
        recyclerViewHistoy.setLayoutManager(mLayoutManager);
        for (int i = 0; i < 2; i++){
            History history = new History();
            if(i == 0) history.setSplite(true);
            history.setTimeSp(App.getApp().getOfftime().getDate());
            history.setTop(App.getApp().getAppName());
            history.setBottom(App.getApp().getOfftime().getLength());
            histories.add(history);
        }

        mAdapter = new ActivityHistoryAdapter(this, histories);
        recyclerViewHistoy.setAdapter(mAdapter);
    }
    private void uploadTime(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        OffTime offtime = App.getApp().getOfftime();


        HttpHelper.NetService service = retrofit.create(HttpHelper.NetService.class);
        retrofit2.Call<OffTime> call = service.createOffTime(offtime);
        call.enqueue(new Callback<OffTime>() {
            @Override
            public void onResponse(retrofit2.Call<OffTime> call, Response<OffTime> response) {
                Log.i("TIME", response.code()+","+response.body());
            }

            @Override
            public void onFailure(retrofit2.Call<OffTime> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.d("","the call is canceled , " + toString());
                } else {
                    Log.e("",t.toString());
                }
            }
        });

    }

    private void uploadApp(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AppUsage app = App.getApp().getAppUsage();

        HttpHelper.NetService service = retrofit.create(HttpHelper.NetService.class);
        retrofit2.Call<AppUsage> call = service.createAppUsage(app);
        call.enqueue(new Callback<AppUsage>() {
            @Override
            public void onResponse(retrofit2.Call<AppUsage> call, Response<AppUsage> response) {
                Log.i("App", response.code()+","+response.body());
            }

            @Override
            public void onFailure(retrofit2.Call<AppUsage> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.d("","the call is canceled , " + toString());
                } else {
                    Log.e("",t.toString());
                }
            }
        });

    }

}

package com.android.elec5619.phoneaway.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.android.elec5619.phoneaway.fragment.ChartFragment;
import com.android.elec5619.phoneaway.fragment.ChartFragmentMonth;
import com.android.elec5619.phoneaway.fragment.ChartFragmentWeek;

/**
 * Created by ASUS on 2016/9/26.
 */

public class CFragmentPagerAdapter extends android.support.v4.app.FragmentPagerAdapter{
    final int PAGE_COUNT = 3;
    public static int PAGE = 0;
    private String tabTitles[] = new String[]{"Day","Week","Month"};
    private Context context;

    public CFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ChartFragment();
            case 1:
                return new ChartFragmentWeek();
            case 2:
                return new ChartFragmentMonth();
        }
        return null;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}

package com.android.elec5619.phoneaway.activity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.elec5619.phoneaway.App;
import com.android.elec5619.phoneaway.R;
import com.android.elec5619.phoneaway.domain.Step;
import com.android.elec5619.phoneaway.net.HttpHelper;
import com.android.elec5619.phoneaway.util.Pedometer;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StepActivity extends BaseActivity implements OnMapReadyCallback {
    @Bind(R.id.textViewStep)
    TextView textViewStep;

    @Bind(R.id.textViewStepCount)
    TextView textViewStepCount;

    @Bind(R.id.textViewCoutner)
    TextView textViewStepCounter;

    @Bind(R.id.textViewMiles)
    TextView textViewDistance;

    private GoogleMap map;
    PolylineOptions options = new PolylineOptions();

    private LocationManager locationManager;

    private Thread thread;
    private float total_step = 0;
    private float init_step = 0;
    Pedometer pedometer;
    private boolean isStop = false;
    private double latitude = 0.0;
    private double longitude = 0.0;
    private boolean isFirst = true;
    private long time = 0;

    List<Double> lats = new ArrayList<>();
    List<Double> lngs = new ArrayList<>();

    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            total_step = pedometer.getStepCount();
            if (isFirst) {
                init_step = total_step;
                time = 0;
                isFirst = false;
            }
            time++;
            long hour = time / 3600;
            long minute = time / 60;
            long second = time % 3600 % 60 ;

            if(hour == 0 && minute ==0){
                textViewStepCounter.setText(minute+"m:"+second +"s");
            }else{
                textViewStepCounter.setText(hour+"h:"+minute+"m:"+second+"s");
            }

            textViewDistance.setText((total_step - init_step)*0.64 +"m");
            textViewStep.setText(String.valueOf(total_step - init_step));

        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step);
        ButterKnife.bind(this);
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);

    }

    private void init() {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Location location = locationManager
                .getLastKnownLocation(LocationManager.GPS_PROVIDER);

        updateView(location);

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                2000, 8, new LocationListener() {

                    @Override
                    public void onLocationChanged(Location location) {

                        Log.e("Map", "Location changed : Lat: "
                                + location.getLatitude() + " Lng: "
                                + location.getLongitude());
                        updateView(location);
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {

                    }

                    @Override
                    public void onProviderDisabled(String provider) {
                        updateView(null);
                    }

                    @Override
                    public void onProviderEnabled(String provider) {

                        if (ActivityCompat.checkSelfPermission(StepActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(StepActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(StepActivity.this,
                                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                                    1);
                            return;
                        }
                        updateView(locationManager.getLastKnownLocation(provider));

                    }
                });

    }

    private void updateView(Location location){
        if(location != null){
            map.addPolyline(new PolylineOptions().geodesic(true)
                .add(new LatLng(location.getLatitude(), location.getLongitude()))  // Sydney

        );
        }
    }

    @OnClick(R.id.textViewStepCount)
    public void onSetpCountClick(){
        if(!isStop){
            pedometer = new Pedometer(this);
            pedometer.register();
            startThread();
            textViewStepCount.setText("Stop");
            isStop = true;
        }else{

            textViewStepCount.setText("Start");
            pedometer.unRegister();
            if(thread != null){
                thread.interrupt();
                thread = null;
            }
            isStop = false;
            isFirst = true;
            map.addPolyline(options.geodesic(true));
            try {
                uploadSteps();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }

    private void startThread() {
        if (thread == null) {

            thread = new Thread(new Runnable() {
                public void run() {
                    while (true) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            break;
                        }

                        Message msg = new Message();
                        handler.sendMessage(msg);

                    }
                }
            });
            thread.start();
        }
    }

    private void uploadSteps() throws ParseException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Step step =  new Step();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        step.setDate(sdf.format(date));
        step.setSteps(String.valueOf(total_step - init_step));
        step.setUid(App.getApp().getUserId());

        HttpHelper.NetService service = retrofit.create(HttpHelper.NetService.class);
        retrofit2.Call<Step> call = service.createStep(step);
        call.enqueue(new Callback<Step>() {
            @Override
            public void onResponse(Call<Step> call, Response<Step> response) {
                if(response.code() == 200){
                    Log.i("Step", response.code()+","+response.body());
                }
            }

            @Override
            public void onFailure(Call<Step> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.d("","the call is canceled , " + toString());
                } else {
                    Log.e("",t.toString());
                }
            }
        });

    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(-33.866, 151.195), 10));

//         Polylines are useful for marking paths and routes on the map.
//        googleMap.addPolyline(new PolylineOptions().geodesic(true)
//                .add(new LatLng(-33.866, 151.195))  // Sydney
//                .add(new LatLng(-18.142, 178.431))  // Fiji
//                .add(new LatLng(21.291, -157.821))  // Hawaii
//                .add(new LatLng(37.423, -122.091))  // Mountain View
//        );
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if(pedometer != null)
        pedometer.unRegister();
        if(thread != null){
            thread.interrupt();
            thread = null;
        }


    }

}

package com.android.elec5619.phoneaway.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.android.elec5619.phoneaway.R;
import com.android.elec5619.phoneaway.adapter.CFragmentPagerAdapter;
import com.android.elec5619.phoneaway.adapter.PieFragmentPagerAdapter;
import com.android.elec5619.phoneaway.fragment.PieChartFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StatisticActivity extends FragmentActivity {

    private CFragmentPagerAdapter fAdapter;
    private PieFragmentPagerAdapter pAdapter;

    @Bind(R.id.tabLayout)
    TabLayout mTabLayout;

    @Bind(R.id.tabLayoutUsage)
    TabLayout mTabLayoutUsage;

    @Bind(R.id.viewpager)
    ViewPager mViewPager;

    @Bind(R.id.viewpagerUsage)
    ViewPager mViewPagerUsage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic);
        ButterKnife.bind(this);

        init();
    }

    private void init(){

        fAdapter = new CFragmentPagerAdapter(getSupportFragmentManager(), this);
        mViewPager.setAdapter(fAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mTabLayout.setTabMode(TabLayout.MODE_FIXED);
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                mViewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        pAdapter = new PieFragmentPagerAdapter(getSupportFragmentManager(), this);
        mViewPagerUsage.setAdapter(pAdapter);
        mTabLayoutUsage.setupWithViewPager(mViewPagerUsage);
        mTabLayoutUsage.setTabMode(TabLayout.MODE_FIXED);

    }

    private void goTimePage(){
        mTabLayout.setVisibility(View.VISIBLE);
        mViewPager.setVisibility(View.VISIBLE);
        mTabLayoutUsage.setVisibility(View.GONE);
        mViewPagerUsage.setVisibility(View.GONE);
    }

    private void goUsagePage(){
        mTabLayout.setVisibility(View.GONE);
        mViewPager.setVisibility(View.GONE);
        mTabLayoutUsage.setVisibility(View.VISIBLE);
        mViewPagerUsage.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.textViewOff)
    public void onOffClick(){
        goTimePage();
    }

    @OnClick(R.id.textViewUsage)
    public void onUsageClick(){
        goUsagePage();
    }
}

package com.android.elec5619.phoneaway.net;

import com.android.elec5619.phoneaway.BuildConfig;
import com.android.elec5619.phoneaway.domain.AppUsage;
import com.android.elec5619.phoneaway.domain.OffTime;
import com.android.elec5619.phoneaway.domain.Step;
import com.android.elec5619.phoneaway.domain.User;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;

/**
 * Created by ASUS on 2016/8/27.
 */
public class HttpHelper {

    public interface NetService {
        @POST("login/getUser")
        Call<User> login(@Body User user);

        @POST("offtime/add")
        Call<OffTime> createOffTime(@Body OffTime offTime);

        @GET("offtime/getTime")
        Call<OffTime> getOffTime(@Body OffTime offTime);

        @POST("offtime/queryTimeList")
        Call<OffTime> getOffTimeList(@Body OffTime offTime);

        @POST("app/add")
        Call<AppUsage> createAppUsage(@Body AppUsage app);

        @GET("app/getApp")
        Call<AppUsage> getAppUsage(@Body AppUsage app);

        @POST("app/queryAppList")
        Call<AppUsage> getAppUsageList(@Body AppUsage app);

        @POST("step/add")
        Call<Step> createStep(@Body Step step);


    }

}

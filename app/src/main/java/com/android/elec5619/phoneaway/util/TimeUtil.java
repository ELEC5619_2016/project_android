package com.android.elec5619.phoneaway.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ASUS on 2016/10/17.
 */

public class TimeUtil {

    public static String getStartTime() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, - 7);
        Date d = c.getTime();
        String preMonday = sdf.format(d);
        return preMonday;
    }

    public static String getEndTime() throws ParseException{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Calendar c = Calendar.getInstance();
        Date d = c.getTime();
        String now = sdf.format(d);
        return now;
    }

    public static String getMonthTime() throws ParseException{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, - 30);
        Date d = c.getTime();
        String now = sdf.format(d);
        return now;
    }
}

package com.android.elec5619.phoneaway;

import android.app.Application;

import com.android.elec5619.phoneaway.domain.AppUsage;
import com.android.elec5619.phoneaway.domain.OffTime;

/**
 * Created by ASUS on 2016/10/17.
 */

public class App extends Application {
    private static App app;
    private String mLength;
    private String mAppName;
    private String mUserId;
    private OffTime offtime;
    private AppUsage appUsage;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
    }

    public static App getApp() {
        return app;
    }

    public void setLength(String length) {
        this.mLength = length;
    }

    public String getLength() {
        return mLength;
    }

    public void setAppName(String app) {
        this.mAppName = app;
    }

    public String getAppName() {
        return mAppName;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public OffTime getOfftime() {
        return offtime;
    }

    public void setOfftime(OffTime offtime) {
        this.offtime = offtime;
    }

    public AppUsage getAppUsage() {
        return appUsage;
    }

    public void setAppUsage(AppUsage appUsage) {
        this.appUsage = appUsage;
    }
}

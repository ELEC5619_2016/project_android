package com.android.elec5619.phoneaway.activity;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.elec5619.phoneaway.App;
import com.android.elec5619.phoneaway.domain.AppUsage;
import com.android.elec5619.phoneaway.domain.History;
import com.android.elec5619.phoneaway.domain.OffTime;
import com.android.elec5619.phoneaway.service.AppStatusService;
import com.android.elec5619.phoneaway.util.BackgroundUtil;
import com.android.elec5619.phoneaway.view.CircleProgressBar;
import com.android.elec5619.phoneaway.R;
import com.github.clans.fab.FloatingActionMenu;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private static CircleProgressBar circleProgressBar;
    private static int progress = 0;
    private static int max = CircleProgressBar.max; // 30mins

    private int hour, minute, second;
    private boolean isStop = false;
    private long length;
    private String app;
    private boolean isFinish = false;

    @Bind(R.id.imageViewPlayStop)
    ImageView imageViewPlayStop;

    @Bind(R.id.textViewTime)
    TextView textViewTime;

    @Bind(R.id.textViewWelcome)
    TextView textViewWelcome;

    FloatingActionMenu floatingActionMenu;
    ProgressAnimation progressAnimation;
    AppStatusService appStatusService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        circleProgressBar = (CircleProgressBar) findViewById(R.id.roundProgressBar);
        floatingActionMenu = (FloatingActionMenu) findViewById(R.id.menu);
        floatingActionMenu.setRotation(0);

        if (BackgroundUtil.HavaPermissionForTest(this) == false) {
            Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            Toast.makeText(this, "Permission is not allowed", Toast.LENGTH_SHORT).show();
        }
        init();
        BackgroundUtil.getForegroundApp(getApplicationContext());
//        Log.i("PHONEAWAY", BackgroundUtil.getForegroundApp(getApplicationContext()));

    }

    private void init(){

        textViewWelcome.setText("Welcome "+getIntent().getStringExtra("username"));
        resetTime();
        if(hour <= 0){
            textViewTime.setText(minute + " mins:" + second + " secs");
        }else{
            textViewTime.setText(hour+" hour:"+ minute + " mins:" + second + " secs");
        }

    }

    private void resetTime(){
        hour = circleProgressBar.getMax() / 3600;
        minute = circleProgressBar.getMax() % 3600 / 60;
        second = circleProgressBar.getMax() % 3600 % 60 ;
    }

    private void bind() {
        Intent intent = new Intent(this, AppStatusService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        Log.i("AppStatusService", "bind()");
    }

    private void unbind() {
        if (appStatusService != null ) {
            unbindService(serviceConnection);
            Log.i("AppStatusService", "unbind()");
        }
    }
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            appStatusService = ((AppStatusService.MyBinder) service).getService();
            appStatusService.setOnGetForegroundApp(new AppStatusService.GetForegroundApp() {
                @Override
                public void GetPackageName(String packagename) {
                    Log.i("Pakcage Name", ""+ packagename);
                    if(packagename != null){
                        if(!packagename.equals("com.android.elec5619.stayfocus")){
                            isFinish= true;
                            app = packagename;
//                            unbind();
                            OffTime offTime = new OffTime();
                            offTime.setLength(String.valueOf(length));
                            offTime.setDone("1");
                            Date date = new Date();
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                            offTime.setDate(sdf.format(date));
                            offTime.setUid(App.getApp().getUserId());
                            App.getApp().setOfftime(offTime);

                            AppUsage app = new AppUsage();
                            app.setAdate(sdf.format(date));
                            app.setUid(App.getApp().getUserId());
                            app.setAname(packagename);

                            App.getApp().setAppUsage(app);
                            App.getApp().setAppName(packagename);

                        }
                    }

                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName name){
        }

    };

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg){
            computeTime();
            switch (msg.what) {
                case 1:
                    if(hour <= 0){
                        textViewTime.setText(minute + " mins:" + second + " secs");
                    }else{
                        textViewTime.setText(hour+" hour:"+ minute + " mins:" + second + " secs");
                    }

            }
        }
    };



    private void computeTime() {
        second--;
        if (second < 0) {
            minute--;
            second = 59;
            if (minute < 0) {
                minute = 59;
                hour--;
            }

        }

    }
    class ProgressAnimation extends AsyncTask<Void, Integer, Void> {
        @Override
        protected void onPreExecute() {
            length = 0;
            resetTime();
        }
        @Override
        protected Void doInBackground(Void... params) {

            for (int i = progress; i < max; i++) {
                if(isCancelled()) break;
                try {
                    length++;
                    publishProgress(i);
                    Thread.sleep(1000);
                    Message message = new Message();
                    message.what = 1;
                    handler.sendMessage(message);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

            circleProgressBar.setProgress(values[0]);
            circleProgressBar.invalidate();
            super.onProgressUpdate(values);
        }
    }

    @OnClick(R.id.fab_step)
    public void onStepClick(){
        Intent intent = new Intent(this, StepActivity .class);
        startActivity(intent);
    }

    @OnClick(R.id.fab_statistic)
    public void onStatisticClick(){
        Intent intent = new Intent(this, StatisticActivity .class);
        startActivity(intent);
    }

    @OnClick(R.id.fab_setting)
    public void onSettingClick(){
        Intent intent = new Intent(this, SettingActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.imageViewPlayStop)
    public void onPlayStopClick(){
        if(isStop){
            timeStop();
            unbind();

            OffTime offTime = new OffTime();
            offTime.setLength(String.valueOf(length));
            offTime.setDone("0");
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            offTime.setDate(sdf.format(date));
            offTime.setUid(App.getApp().getUserId());
            App.getApp().setOfftime(offTime);
            App.getApp().setAppName(app);
            App.getApp().setAppUsage(null);
            Intent intent = new Intent(this, HistoryActivity.class);
            startActivity(intent);
        }else{
            bind();
            imageViewPlayStop.setImageResource(R.drawable.icon_stop);
            progressAnimation =  new ProgressAnimation();
            progressAnimation.execute();
            isStop = true;
        }
    }

    private void timeStop(){
        imageViewPlayStop.setImageResource(R.drawable.icon_play);
        if(progressAnimation != null)
        progressAnimation.cancel(true);
        isStop = false;

    }
    @Override
    public void onResume(){
        super.onResume();
        resetTime();
        if(hour <= 0){
            textViewTime.setText(minute + " mins:" + second + " secs");
        }else{
            textViewTime.setText(hour+" hour:"+ minute + " mins:" + second + " secs");
        }
        if(isFinish){
            timeStop();
            if(isServiceRunning(this, "com.android.elec5619.stayfocus"))
                unbind();
        }


    }
    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    public static boolean isServiceRunning(Context mContext,String className) {
        boolean isRunning = false;
        ActivityManager activityManager = (ActivityManager)
                mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> serviceList
                = activityManager.getRunningServices(30);
        if (!(serviceList.size()>0)) {
            return false;
        }
        for (int i=0; i<serviceList.size(); i++) {
            if (serviceList.get(i).service.getClassName().equals(className) == true) {
                isRunning = true;
                break;
            }
        }
        return isRunning;
    }
}
